Si has instalado Office en Archivos de programa (generalmente en el disco C), entonces ejecuta alguno de los siguientes comandos dependiendo de la arquitectura (32 o 64 bits). Si no sabes qué arquitectura es, entonces ejecuta ambos. Uno mostrará error, pero el otro funcionará:

cd /d %ProgramFiles%\Microsoft Office\Office16
cd /d %ProgramFiles(x86)%\Microsoft Office\Office16

3. Convertir tu versión RETAIL a VL (Licencia por Volumen)

Para ello, ejecuta el siguiente comando. Si ya tienes una versión por volumen puedes omitir este paso.

for /f %x in ('dir /b ..\root\Licenses16\proplusvl_kms*.xrm-ms') do cscript ospp.vbs /inslic:"..\root\Licenses16\%x"

4. Finalmente, instala la clave KMS ejecutando este código en el CMD.

cscript ospp.vbs /inpkey:XQNVK-8JYDB-WJ9W3-YJ8YR-WFG99

cscript ospp.vbs /unpkey:BTDRB >nul

cscript ospp.vbs /unpkey:KHGM9 >nul

cscript ospp.vbs /unpkey:CPQVG >nul

cscript ospp.vbs /sethst:kms8.msguides.com

cscript ospp.vbs /setprt:1688

cscript ospp.vbs /act